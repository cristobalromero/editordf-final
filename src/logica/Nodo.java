/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.ArrayList;

/**
 *
 * @author katherine
 */
public class Nodo {

    private ArrayList<Nodo> neighbors;
    private String code;
    private String type;
    private boolean start = false;
    private boolean finish = false;

    public Nodo() {
        neighbors = new ArrayList<>();
        type = "";
    }

    public Nodo(String code) {
        this.code = code;
        neighbors = new ArrayList<>();
        type = "";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isStart() {
        return start;
    }

    public void setStart(boolean start) {
        this.start = start;
        if (start) {
            this.finish = false;
        }
    }

    public boolean isFinish() {
        return finish;
    }

    public void setFinish(boolean finish) {
        this.finish = finish;
        if (finish) {
            this.start = false;
        }
    }

    public ArrayList<Nodo> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(ArrayList<Nodo> neighbor) {
        this.neighbors = neighbor;
    }

    public void addNeighbor(Nodo neighbor) {
        this.neighbors.add(neighbor);
    }

}
