/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import com.mxgraph.model.mxCell;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author familia
 */
public class Grafo {

    private static HashMap<String, Nodo> grafo = new HashMap<>();

    public Grafo() {
        super();
    }

    public static void parseGrafo(ArrayList<mxCell> blocks) {
//        ArrayList<Nodo> nodos = new ArrayList<>();
        HashMap<String, Nodo> nodos = new HashMap<>();
        Iterator<mxCell> it = blocks.iterator();
        int countId = 0;
        while (it.hasNext()) {
            mxCell itNext = it.next();
            nodos.put(countId + "", new Nodo((String) itNext.getValue()));
            itNext.setId(countId + "");
            countId++;
        }
        //recorrido para comprobar si tiene vecinos del array de bloque y hacer vecinos a sus equivalente en el array de Nodos
        Iterator<mxCell> bloqueRaiz = blocks.iterator();
        while (bloqueRaiz.hasNext()) {
            System.out.println("recorriendopadre");
            mxCell raizNext = bloqueRaiz.next();
            for (int i = 0; i < raizNext.getEdgeCount(); i++) {
                Iterator<mxCell> bloqueVecino = blocks.iterator();
                while (bloqueVecino.hasNext()) {
                    mxCell vecinoNext = bloqueVecino.next();
                    for (int j = 0; j < vecinoNext.getEdgeCount(); j++) {
                        System.out.println("buscando hijos");
                        if (raizNext.getEdgeAt(i).equals(bloqueVecino.next().getEdgeAt(j))) {
                            System.out.println("son vecinos");
                            nodos.get(raizNext.getId()).addNeighbor(nodos.get(vecinoNext.getId()));
                        }
                    }
                }
            }
        }
        Iterator iterator = nodos.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Nodo> e = (Map.Entry) iterator.next();
            System.out.println(e.getKey() + " " + e.getValue().getNeighbors().size());
        }
    }

    public static void addNodo(mxCell block) {
        if (grafo.get(block.getId()) == null) {
            grafo.put(block.getId(), new Nodo((String) block.getValue()));
        }
    }

    public static void relatingBlock(mxCell cola, mxCell cabeza) {
        if (grafo.get(cola.getId()) == null) {
            grafo.put(cola.getId(), new Nodo((String) cola.getValue()));
            if (grafo.get(cabeza.getId()) == null) {
                grafo.put(cabeza.getId(), new Nodo((String) cabeza.getValue()));
                grafo.get(cola.getId()).addNeighbor(grafo.get(cabeza.getId()));
//                System.out.println("prueba1");
            } else {
                grafo.get(cola.getId()).addNeighbor(grafo.get(cabeza.getId()));
//                System.out.println("prueba2");
            }
        } else if (grafo.get(cabeza.getId()) == null) {
            grafo.put(cabeza.getId(), new Nodo((String) cabeza.getValue()));
            grafo.get(cola.getId()).addNeighbor(grafo.get(cabeza.getId()));
//            System.out.println("prueba3");
        } else {
            grafo.get(cola.getId()).addNeighbor(grafo.get(cabeza.getId()));
        }
    }

    public static void prueba() {
        Iterator it = grafo.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Nodo> e = (Map.Entry) it.next();
            System.out.println(e.getKey() + " - " + e.getValue().getNeighbors().size());
        }
    }
}
