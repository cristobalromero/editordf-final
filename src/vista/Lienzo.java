/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import logica.Grafo;

/**
 *
 * @author katherine
 */
public class Lienzo extends javax.swing.JPanel {

    public static int TYPE_PROCESO = 1;

    private mxGraph graph;
    private mxGraphComponent graphComponent;
    private boolean anchoring, deleting, relating;
    private int typeBlock = 0, contId = 0;
    private mxCell cola = null;//indicara quien es la cola al momento de hacer una relacion
    private mxCell blockFirst, blockLast;//indican el primer y ultimo elemento.
    private ArrayList<mxCell> blocks;

    /**
     * Creates new form Lienzo
     */
    public Lienzo() {
        initComponents();
        initGUI();
    }

    private void initGUI() {
        this.setBackground(Color.black);
        graph = new mxGraph();
        graphComponent = new mxGraphComponent(graph);
        anchoring = false;
        relating = false;
        deleting = false;
        blocks = new ArrayList<>();

        this.add(graphComponent);
        graphComponent.setLocation(0, 0);
        adjustDimension();
        //añadiento inicio y fin
        graph.getModel().beginUpdate();
        Object parent = graph.getDefaultParent();
        blockFirst = (mxCell) graph.insertVertex(parent, null, "Inicio", 50, 50, 100, 50);
        blockFirst.setConnectable(false);
        blockFirst.setId(contId + "");
        contId++;
        Grafo.addNodo(blockFirst);
        blockLast = (mxCell) graph.insertVertex(parent, null, "Fin", 50, 150, 100, 50);
        blockLast.setConnectable(false);
        blockLast.setId(contId + "");
        contId++;
        Grafo.addNodo(blockLast);
        graph.getModel().endUpdate();
        blocks.add(blockFirst);
        blocks.add(blockLast);
        //añadiento inicio y fin
        graphComponent.getGraphControl().addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                if (anchoring) {
                    anchorBlock(event.getX(), event.getY());
                    return;
                }
                if (graphComponent.getCellAt(event.getX(), event.getY()) instanceof mxCell) {
                    mxCell blockSel = (mxCell) graphComponent.getCellAt(event.getX(), event.getY());
                    if (blockSel.isEdge()) {
                        graph.setCellsLocked(true);
                    } else {
                        graph.setCellsLocked(false);
                    }
                    if (deleting) {
                        deleting(new Object[]{blockSel});
                    } else if (relating) {
                        if (cola != null) {
                            relatingBlock(cola, blockSel);
                            cola = null;
                            relating = false;
                        } else if (!blockSel.isEdge()) {
                            cola = blockSel;
                        } else {
                            relating = false;
                        }
                    }
                } else {
                    graph.setCellsLocked(false);
                }
            }
        });
    }

    public void adjustDimension() {
        graphComponent.setSize(this.getSize());
        graphComponent.setPreferredSize(this.preferredSize());
    }

    /**
     * add a static varible to the canvas
     *
     * @param type is the block type, use static varibles of this class
     */
    public void addblock(int type) {
        if (type > 0) {
            typeBlock = type;
            anchoring = true;
            this.deleting = false;
            this.relating = false;
            cola = null;
        } else {
            this.anchoring = false;
            this.deleting = false;
            this.relating = false;
            JOptionPane.showMessageDialog(this, "El elemento seleccionado es erroneo");
        }
    }

    /**
     * ancla el block en la posicion del lienzo donde se halla realizado un
     * click
     *
     * @param x position x
     * @param y position y
     */
    public void anchorBlock(int x, int y) {
        switch (typeBlock) {
            case 1://proceso
                graph.getModel().beginUpdate();
                Object parent = graph.getDefaultParent();
                mxCell block = (mxCell) graph.insertVertex(parent, null, "", x, y, 100, 50);
                block.setConnectable(false);
                block.setId(contId + "");
                contId++;
                Grafo.addNodo(block);
                blocks.add(block);
                graph.getModel().endUpdate();
                break;
        }
        typeBlock = 0;
        anchoring = false;
    }

    public void relatingBlock(mxCell cola, mxCell cabeza) {
        Object parent = graph.getDefaultParent();
        graph.insertEdge(parent, null, "", cola, cabeza);
        Grafo.relatingBlock(cola, cabeza);
        relating = false;
    }

    public void relatingBlock(mxCell cola, mxCell cabeza, String nombre) {
        graph.insertEdge(graph.getDefaultParent(), null, nombre, cola, cabeza);
    }

    public void deleting(Object[] elementos) {//hay que hacer un metodo en grafo que elimine las relaciones
        graph.removeCells(elementos);
        deleting = false;
    }

    public void generarGrafo() {
//        Grafo.parseGrafo(blocks);
        Grafo.prueba();
    }
//sets, gets

    public boolean isAnchoring() {
        return anchoring;
    }

    public boolean isDeleting() {
        return deleting;
    }

    public void setDeleting(boolean deleting) {
        this.deleting = deleting;
        this.anchoring = false;
        this.relating = false;
        cola = null;
    }

    public boolean isRelating() {
        return relating;
    }

    public void setRelating(boolean relating) {
        this.relating = relating;
        this.anchoring = false;
        this.deleting = false;
        cola = null;
    }

    //fin gets sets
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
